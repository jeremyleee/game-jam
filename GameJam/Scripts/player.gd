extends KinematicBody2D

export var speed =10.0
export var tileSize=32.0
onready var sprite = $Sprite

export var initpos = Vector2()
var dir =Vector2()
var counter=0.0
var moving = false
var facing = "down"
export var Ruby = 0
export var destination =Vector2(0,0)

func _ready():
	set_process(true)
	initpos=position
	
func _process(delta):
	var label=get_parent().get_node("RubyCounter/UI/base/textlabel")
	label.text= str (Ruby)
	
	if not moving:
		set_dir()
	elif dir != Vector2():
		move(delta)
	else :
		moving =false
		
	if facing =="down":
		sprite.frame = 0
	elif facing == "up":
		sprite.frame = 12
	elif facing =="left":
		sprite.frame = 4
	elif facing =="right":
		sprite.frame = 8

func set_dir():
	dir = get_dir()
	if dir.x > 0:
		facing="right"
	elif dir.x < 0:
		facing="left"
	elif dir.y > 0:
		facing="down"
	elif dir.y < 0:
		facing="up"
			
	if dir.x!=0 or dir.y!=0:
		moving = true
		initpos = position

func get_dir():
	var x=0
	var y=0
	
	if dir.y==0:
		x=int(Input.is_action_pressed("ui_right"))-int(Input.is_action_pressed("ui_left"))
	if dir.x==0:
		y=int(Input.is_action_pressed("ui_down"))-int(Input.is_action_pressed("ui_up"))
	
	return Vector2(x,y)
	
func move(delta):
	counter += delta *speed
	if counter >= 1.0:
		position=initpos+dir*tileSize
		counter=0.0
		moving=false
	else:
		position=initpos+dir*tileSize*counter

func _physics_process(delta):
	get_dir()
	move_and_slide(dir)

func _on_Area2D_body_entered(body):
	Ruby+=1
	pass 


func _on_Trap_body_entered(body):
	get_tree().change_scene(str("res://Scenes/Game.tscn"))
	#body.position = get_parent().get_node("areaawal").get_global_pos()
	pass


func _on_button_body_entered(body):

	pass 
